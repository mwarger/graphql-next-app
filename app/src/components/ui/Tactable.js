export default () => (
  <a
    css={`
      position: fixed;
      bottom: 0px;
      left: 0px;
      text-decoration: none;
      color: black;
      padding: 20px;
      font-size: 14px;
    `}
    href="https://tactable.io"
    target="_blank"
  >
    ✌️ Tactable.io
  </a>
)
