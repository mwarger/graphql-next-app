const jwtImplementation = require("@jwt/implementation")

const JwtFactory = {
  createTokenWithPayload(payload) {
    const secret = process.env.JWT_SECRET || "secret"
    const algorithm = "HS256"
    const token = jwtImplementation.sign(payload, secret, algorithm)

    return token
  }
}

module.exports = JwtFactory
